#!/bin/bash

# builds a cscope database for use vim

# just .c and .h
#find . -name "*.[ch]" -print > cscope.files

# .cpp, .c, .h and .hpp
find . -regex ".+\.\(cpp\|c\|h\|hpp\)$" > cscope.files
find . -regex ".+\.\(CPP\|C\|H\|HPP\)$" >> cscope.files

# macosx version
#find -E . -regex ".+\.(cpp|c|h|hpp)$" > cscope.files
#find -E . -regex ".+\.(CPP|C|H|HPP)$" >> cscope.files

# linux version

#
# src
#
ctags -a  src/*.c src/*.C src/*.h src/*.H   src/*.hpp src/*.HPP
ctags -a  src/*.c src/*.C src/*.cpp src/*.h src/*.H   src/*.hpp src/*.HPP

ctags -a  src/*/*.c src/*/*.C src/*/*.h src/*/*.H   src/*/*.hpp src/*/*.HPP
ctags -a  src/*/*.c src/*/*.C src/*/*.cpp src/*/*.h src/*/*.H   src/*/*.hpp src/*/*.HPP

ctags -a  src/*/*/*.c src/*/*/*.C src/*/*/*.h src/*/*/*.H   src/*/*/*.hpp src/*/*/*.HPP
ctags -a  src/*/*/*.c src/*/*/*.C src/*/*/*.cpp src/*/*/*.h src/*/*/*.H   src/*/*/*.hpp src/*/*/*.HPP

ctags -a  src/*/*/*/*.c src/*/*/*/*.C src/*/*/*/*.h src/*/*/*/*.H   src/*/*/*/*.hpp src/*/*/*/*.HPP
ctags -a  src/*/*/*/*.c src/*/*/*/*.C src/*/*/*/*.cpp src/*/*/*/*.h src/*/*/*/*.H   src/*/*/*/*.hpp src/*/*/*/*.HPP

ctags -a  src/*/*/*/*/*.c src/*/*/*/*/*.C src/*/*/*/*/*.h src/*/*/*/*/*.H   src/*/*/*/*/*.hpp src/*/*/*/*/*.HPP
ctags -a  src/*/*/*/*/*.c src/*/*/*/*/*.C src/*/*/*/*/*.cpp src/*/*/*/*/*.h src/*/*/*/*/*.H   src/*/*/*/*/*.hpp src/*/*/*/*/*.HPP


ctags -a  src/*/*/*/*/*/*.c src/*/*/*/*/*/*.C src/*/*/*/*/*/*.h src/*/*/*/*/*/*.H   src/*/*/*/*/*/*.hpp src/*/*/*/*/*/*.HPP
ctags -a  src/*/*/*/*/*/*.c src/*/*/*/*/*/*.C src/*/*/*/*/*/*.cpp src/*/*/*/*/*/*.h src/*/*/*/*/*/*.H   src/*/*/*/*/*/*.hpp src/*/*/*/*/*/*.HPP


ctags -a  src/*/*/*/*/*/*/*.c src/*/*/*/*/*/*/*.C src/*/*/*/*/*/*/*.h src/*/*/*/*/*/*/*.H   src/*/*/*/*/*/*/*.hpp src/*/*/*/*/*/*/*.HPP
ctags -a  src/*/*/*/*/*/*/*.c src/*/*/*/*/*/*/*.C src/*/*/*/*/*/*/*.cpp src/*/*/*/*/*/*/*.h src/*/*/*/*/*/*/*.H   src/*/*/*/*/*/*/*.hpp src/*/*/*/*/*/*/*.HPP


ctags -a  src/*/*/*/*/*/*/*/*.c src/*/*/*/*/*/*/*/*.C src/*/*/*/*/*/*/*/*.h src/*/*/*/*/*/*/*/*.H   src/*/*/*/*/*/*/*/*.hpp src/*/*/*/*/*/*/*/*.HPP
ctags -a  src/*/*/*/*/*/*/*/*.c src/*/*/*/*/*/*/*/*.C src/*/*/*/*/*/*/*/*.cpp src/*/*/*/*/*/*/*/*.h src/*/*/*/*/*/*/*/*.H   src/*/*/*/*/*/*/*/*.hpp src/*/*/*/*/*/*/*/*.HPP


#
# build_utils
#
ctags -a  build_utils/*.c build_utils/*.C build_utils/*.h build_utils/*.H   build_utils/*.hpp build_utils/*.HPP
ctags -a  build_utils/*.c build_utils/*.C build_utils/*.cpp build_utils/*.h build_utils/*.H   build_utils/*.hpp build_utils/*.HPP

ctags -a  build_utils/*/*.c build_utils/*/*.C build_utils/*/*.h build_utils/*/*.H   build_utils/*/*.hpp build_utils/*/*.HPP
ctags -a  build_utils/*/*.c build_utils/*/*.C build_utils/*/*.cpp build_utils/*/*.h build_utils/*/*.H   build_utils/*/*.hpp build_utils/*/*.HPP

ctags -a  build_utils/*/*/*.c build_utils/*/*/*.C build_utils/*/*/*.h build_utils/*/*/*.H   build_utils/*/*/*.hpp build_utils/*/*/*.HPP
ctags -a  build_utils/*/*/*.c build_utils/*/*/*.C build_utils/*/*/*.cpp build_utils/*/*/*.h build_utils/*/*/*.H   build_utils/*/*/*.hpp build_utils/*/*/*.HPP

ctags -a  build_utils/*/*/*/*.c build_utils/*/*/*/*.C build_utils/*/*/*/*.h build_utils/*/*/*/*.H   build_utils/*/*/*/*.hpp build_utils/*/*/*/*.HPP
ctags -a  build_utils/*/*/*/*.c build_utils/*/*/*/*.C build_utils/*/*/*/*.cpp build_utils/*/*/*/*.h build_utils/*/*/*/*.H   build_utils/*/*/*/*.hpp build_utils/*/*/*/*.HPP

ctags -a  build_utils/*/*/*/*/*.c build_utils/*/*/*/*/*.C build_utils/*/*/*/*/*.h build_utils/*/*/*/*/*.H   build_utils/*/*/*/*/*.hpp build_utils/*/*/*/*/*.HPP
ctags -a  build_utils/*/*/*/*/*.c build_utils/*/*/*/*/*.C build_utils/*/*/*/*/*.cpp build_utils/*/*/*/*/*.h build_utils/*/*/*/*/*.H   build_utils/*/*/*/*/*.hpp build_utils/*/*/*/*/*.HPP


ctags -a  build_utils/*/*/*/*/*/*.c build_utils/*/*/*/*/*/*.C build_utils/*/*/*/*/*/*.h build_utils/*/*/*/*/*/*.H   build_utils/*/*/*/*/*/*.hpp build_utils/*/*/*/*/*/*.HPP
ctags -a  build_utils/*/*/*/*/*/*.c build_utils/*/*/*/*/*/*.C build_utils/*/*/*/*/*/*.cpp build_utils/*/*/*/*/*/*.h build_utils/*/*/*/*/*/*.H   build_utils/*/*/*/*/*/*.hpp build_utils/*/*/*/*/*/*.HPP


ctags -a  build_utils/*/*/*/*/*/*/*.c build_utils/*/*/*/*/*/*/*.C build_utils/*/*/*/*/*/*/*.h build_utils/*/*/*/*/*/*/*.H   build_utils/*/*/*/*/*/*/*.hpp build_utils/*/*/*/*/*/*/*.HPP
ctags -a  build_utils/*/*/*/*/*/*/*.c build_utils/*/*/*/*/*/*/*.C build_utils/*/*/*/*/*/*/*.cpp build_utils/*/*/*/*/*/*/*.h build_utils/*/*/*/*/*/*/*.H   build_utils/*/*/*/*/*/*/*.hpp build_utils/*/*/*/*/*/*/*.HPP


ctags -a  build_utils/*/*/*/*/*/*/*/*.c build_utils/*/*/*/*/*/*/*/*.C build_utils/*/*/*/*/*/*/*/*.h build_utils/*/*/*/*/*/*/*/*.H   build_utils/*/*/*/*/*/*/*/*.hpp build_utils/*/*/*/*/*/*/*/*.HPP
ctags -a  build_utils/*/*/*/*/*/*/*/*.c build_utils/*/*/*/*/*/*/*/*.C build_utils/*/*/*/*/*/*/*/*.cpp build_utils/*/*/*/*/*/*/*/*.h build_utils/*/*/*/*/*/*/*/*.H   build_utils/*/*/*/*/*/*/*/*.hpp build_utils/*/*/*/*/*/*/*/*.HPP



#
# build
#
ctags -a  build/*.c build/*.C build/*.h build/*.H   build/*.hpp build/*.HPP
ctags -a  build/*.c build/*.C build/*.cpp build/*.h build/*.H   build/*.hpp build/*.HPP

ctags -a  build/*/*.c build/*/*.C build/*/*.h build/*/*.H   build/*/*.hpp build/*/*.HPP
ctags -a  build/*/*.c build/*/*.C build/*/*.cpp build/*/*.h build/*/*.H   build/*/*.hpp build/*/*.HPP

ctags -a  build/*/*/*.c build/*/*/*.C build/*/*/*.h build/*/*/*.H   build/*/*/*.hpp build/*/*/*.HPP
ctags -a  build/*/*/*.c build/*/*/*.C build/*/*/*.cpp build/*/*/*.h build/*/*/*.H   build/*/*/*.hpp build/*/*/*.HPP

ctags -a  build/*/*/*/*.c build/*/*/*/*.C build/*/*/*/*.h build/*/*/*/*.H   build/*/*/*/*.hpp build/*/*/*/*.HPP
ctags -a  build/*/*/*/*.c build/*/*/*/*.C build/*/*/*/*.cpp build/*/*/*/*.h build/*/*/*/*.H   build/*/*/*/*.hpp build/*/*/*/*.HPP

ctags -a  build/*/*/*/*/*.c build/*/*/*/*/*.C build/*/*/*/*/*.h build/*/*/*/*/*.H   build/*/*/*/*/*.hpp build/*/*/*/*/*.HPP
ctags -a  build/*/*/*/*/*.c build/*/*/*/*/*.C build/*/*/*/*/*.cpp build/*/*/*/*/*.h build/*/*/*/*/*.H   build/*/*/*/*/*.hpp build/*/*/*/*/*.HPP


ctags -a  build/*/*/*/*/*/*.c build/*/*/*/*/*/*.C build/*/*/*/*/*/*.h build/*/*/*/*/*/*.H   build/*/*/*/*/*/*.hpp build/*/*/*/*/*/*.HPP
ctags -a  build/*/*/*/*/*/*.c build/*/*/*/*/*/*.C build/*/*/*/*/*/*.cpp build/*/*/*/*/*/*.h build/*/*/*/*/*/*.H   build/*/*/*/*/*/*.hpp build/*/*/*/*/*/*.HPP


ctags -a  build/*/*/*/*/*/*/*.c build/*/*/*/*/*/*/*.C build/*/*/*/*/*/*/*.h build/*/*/*/*/*/*/*.H   build/*/*/*/*/*/*/*.hpp build/*/*/*/*/*/*/*.HPP
ctags -a  build/*/*/*/*/*/*/*.c build/*/*/*/*/*/*/*.C build/*/*/*/*/*/*/*.cpp build/*/*/*/*/*/*/*.h build/*/*/*/*/*/*/*.H   build/*/*/*/*/*/*/*.hpp build/*/*/*/*/*/*/*.HPP


ctags -a  build/*/*/*/*/*/*/*/*.c build/*/*/*/*/*/*/*/*.C build/*/*/*/*/*/*/*/*.h build/*/*/*/*/*/*/*/*.H   build/*/*/*/*/*/*/*/*.hpp build/*/*/*/*/*/*/*/*.HPP
ctags -a  build/*/*/*/*/*/*/*/*.c build/*/*/*/*/*/*/*/*.C build/*/*/*/*/*/*/*/*.cpp build/*/*/*/*/*/*/*/*.h build/*/*/*/*/*/*/*/*.H   build/*/*/*/*/*/*/*/*.hpp build/*/*/*/*/*/*/*/*.HPP


# how to do it
# :'<,'>s/\*\./\*\/\*\./gc

# mac version
# I don't know how to do a recursive search, need to add wildcards for dirs
# this does three levels deep
ctags -a  *.cpp  *.hpp *.c *.h
ctags -a  */*.cpp  */*.hpp */*.c */*.h
ctags -a  */*/*.cpp  */*/*.hpp */*/*.c */*/*.h
ctags -a  */*/*/*.cpp  */*/*/*.hpp */*/*/*.c */*/*/*.h
#ctags -a  *.cpp  *.hpp 


#cscope -q -k -b -i cscope.files
echo cscope -q -i cscope.files 

cscope  -R -ssrc -sbuild -sbuild_utils

